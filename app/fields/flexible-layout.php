<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$flexible_layout = new FieldsBuilder('flexible_layout');

// Load header partials

// Load content partials
$spacer = get_field_partial('partials.spacer');
$wysiwyg = get_field_partial('partials.wysiwyg');
$section__image_text = get_field_partial('partials.section__image-text');
$section__image_banner = get_field_partial('partials.section__image-banner');
$section__image_slider = get_field_partial('partials.section__image-slider');
$section__hero = get_field_partial('partials.section__hero');
$section__companies_slider = get_field_partial('partials.section__companies-slider');
$section__contact = get_field_partial('partials.section__contact');
$section__roadmap = get_field_partial('partials.section__roadmap');
$section__images = get_field_partial('partials.section__images');
$section__cases = get_field_partial('partials.section__cases');
$section__circletext = get_field_partial('partials.section__circletext');

$flexible_layout
    ->addFlexibleContent('flex_header', ['button_label' => 'Add Content Row'])

    ->addLayout($spacer)
    ->addLayout($section__hero)

    ->endFlexibleContent()

    ->addFlexibleContent('flex_content', ['button_label' => 'Add Content Row'])

    ->addLayout($spacer)
    ->addLayout($wysiwyg)
    ->addLayout($section__image_text)
    ->addLayout($section__image_banner)
    ->addLayout($section__image_slider)
    ->addLayout($section__companies_slider)
    ->addLayout($section__contact)
    ->addLayout($section__roadmap)
    ->addLayout($section__images)
    ->addLayout($section__cases)
    ->addLayout($section__circletext)

    ->endFlexibleContent();
return $flexible_layout;