<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$section__roadmap = new FieldsBuilder('section__roadmap');
 
$section__roadmap
    ->addGroup('section__roadmap')

        ->addText('title')

        ->addRepeater('roadmap')
            ->addImage('icon')
            ->addWysiwyg('text')
        ->endRepeater()

    ->endGroup();

return $section__roadmap;