<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$section__companies_slider = new FieldsBuilder('section__companies-slider');
 
$section__companies_slider
    ->addGroup('section__companies-slider')

        ->addText('title')

        ->addRepeater('companies')
            ->addImage('logo')
            ->addTextArea('text')
        ->endRepeater()
 

    ->endGroup();

return $section__companies_slider;