<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$section__images = new FieldsBuilder('section__images');
 
$section__images
    ->addGroup('section__images')

        ->addGroup('info')
            ->addText('title')
            ->addWysiwyg('text')
            ->addLink('cta')
        ->endGroup()

        ->addImage('top_image')
            ->setWidth(50)
        ->addImage('right_image')
            ->setWidth(50)
        ->addImage('bottom_image')
            ->setWidth(50)
        ->addImage('left_image')
            ->setWidth(50)

    ->endGroup();

return $section__images;