<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$section__hero = new FieldsBuilder('section__hero');
 
$section__hero
    ->addGroup('section__hero')

        ->addGroup('image')
            ->setWidth(50)
            ->addImage('image_person')
            ->addText('bubble_1')
            ->addText('bubble_2')
        ->endGroup()

        ->addGroup('info')
            ->setWidth(50)
            ->addText('title')
            ->addWysiwyg('text')
            ->addLink('cta')
        ->endGroup()

        ->addGroup('bottom')
            ->addWysiwyg('text_left')
                ->setWidth(50)
            ->addWysiwyg('text_right')
                ->setWidth(50)
        ->endGroup()

    ->endGroup();

return $section__hero;