<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$section__circletext = new FieldsBuilder('section__circletext');
 
$section__circletext
    ->addGroup('section__circletext')

        ->addText('title')
        ->addWysiwyg('text')

    ->endGroup();

return $section__circletext;