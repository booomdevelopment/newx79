<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$section__contact = new FieldsBuilder('section__contact');
 
$section__contact
    ->addGroup('section__contact')

        ->addgroup('info')
            ->setWidth(50)
            ->addText('label')
            ->addText('title')
            ->addWysiwyg('text')
        ->endgroup()
 
        ->addText('form')
            ->setWidth(50)

    ->endGroup();

return $section__contact;