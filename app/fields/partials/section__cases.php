<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$section__cases = new FieldsBuilder('section__cases');
 
$section__cases
    ->addGroup('section__cases')

        ->addText('title')

        ->addRepeater('cases')
            ->addImage('image')
            ->addWysiwyg('text')
        ->endRepeater()

    ->endGroup();

return $section__cases;