@layouts('flex_header')

    @layout('spacer')
    @include('partials.spacer')
    @endlayout

    @layout('section__hero')
    @include('partials.section__hero')
    @endlayout

@endlayouts

@layouts('flex_content')

    @layout('spacer')
    @include('partials.spacer')
    @endlayout

    @layout('wysiwyg')
    @include('partials.wysiwyg')
    @endlayout

    @layout('section__image-text')
    @include('partials.section__image-text')
    @endlayout

    @layout('section__image-banner')
    @include('partials.section__image-banner')
    @endlayout

    @layout('section__image-slider')
    @include('partials.section__image-slider')
    @endlayout

    @layout('section__companies-slider')
    @include('partials.section__companies-slider')
    @endlayout

    @layout('section__contact')
    @include('partials.section__contact')
    @endlayout

    @layout('section__roadmap')
    @include('partials.section__roadmap')
    @endlayout

    @layout('section__images')
    @include('partials.section__images')
    @endlayout

    @layout('section__cases')
    @include('partials.section__cases')
    @endlayout

    @layout('section__circletext')
    @include('partials.section__circletext')
    @endlayout

@endlayouts