@group('section__companies-slider')
<section class='companies-slider'>
        @hassub('title')
            <h2 class='title'>
                @sub('title')
            </h2>
        @endsub
    <div class="companies-slider__wrap container">
        <div class="companies-slider__swiper">
            <div class="swiper-wrapper">
                @fields('companies')
                    <div class="swiper-slide">
                        @hassub('logo')
                        <div class="logo">
                            <img src='@sub('logo', 'url')'>
                        </div>
                        @endsub
                        @hassub('text')
                            <div class='text'>
                                @sub('text')
                            </div>
                        @endsub
                    </div>
                @endfields
            </div>
        </div>
        <div class="companies-nav mt-12 flex items-center">
            <div class="companies-prev mr-12"><img src="@asset('images/left.svg')" alt=""></div>
            <div class="companies-next"><img src="@asset('images/right.svg')" alt=""></div>
        </div>
    </div>
</section>
@endgroup