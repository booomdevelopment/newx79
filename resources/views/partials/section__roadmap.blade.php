@group('section__roadmap')
    <section class='roadmap'>
        <div class='roadmap__wrap container'>
            @hassub('title')
                <h2 class='title text-center'>
                    @sub('title')
                </h2>
            @endsub
            <div class="roadmap__map">
                @fields('roadmap')
                    <div class="roadmap__item">
                        @hassub('icon')
                        <div class="circles circleimg">
                            <div class="circles">
                                <div class="circles">
                                    <img src='@sub('icon', 'url')' class='icon'>
                                </div>
                            </div>
                        </div>
                        @endsub
                        @hassub('text')
                            <div class='text'>
                                @sub('text')
                            </div>
                        @endsub
                    </div>
                @endfields
            </div>
        </div>
    </section>
@endgroup