@group('section__hero')
<section class="hero-intro">
    <div class="top-section container flex flex-row justify-between items-end">
        @group('info')
        <div class="left-text">
            @hassub('title')
                <h1 class='title'>
                    @sub('title')
                </h1>
            @endsub
     
                <div class="content flex flex-row justify-start items-start">
                    @hassub('text')
                        <div class='ext text--line'>
                            @sub('text')
                        </div>
                    @endsub 
                </div>
                @hassub('cta')
                    <a href='@sub('cta', 'url')' class='btn btn--image-arrow ml-16'>
                        @hasoption('person')
                            <img src="@option('person', 'url')" class="person" alt="">
                        @endoption
                        @sub('cta', 'title')
                        <img src="@asset('images/arrowwhite.svg')" alt="">
                    </a>
                @endsub
        </div>
        @endgroup
        @group('image')
        <div class="right-visual">
            @hassub('bubble_1')
                <div class="speech-bubble sp-1">
                    <span>@sub('bubble_1')</span>
                </div>
            @endsub
            @hassub('bubble_2')
                <div class="speech-bubble sp-2">
                    <span>@sub('bubble_2')</span>
                </div>
            @endsub
            @hassub('image_person')
                <img src=" @sub('image_person', 'url')" alt="">
            @endsub
        </div>
        @endgroup
    </div>
    @group('bottom')
    <div class="bottom-section">
        <div class="container flex flex-row justify-between">
            @hassub('text_left')
                <div class='left'>
                    @sub('text_left')
                </div>
            @endsub
            @hassub('text_right')
                <div class='right'>
                    @sub('text_right')
                </div>
            @endsub
       
        </div>
    </div>
    @endgroup
</section>
@endgroup