<header class="banner">
  <div class="container banner__wrap">
      <img class="custom-logo" src="@asset('images/logo-x79.png')" alt="">
    <nav class="nav-primary">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif
    </nav>
    <a href="#contact" class="btn btn--arrow">Start vandaag <img src="@asset('images/arrowwhite.svg')" alt=""></a>
  </div>
</header>
