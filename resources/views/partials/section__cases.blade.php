@group('section__cases')
    <section class='cases'>
        <div class='cases__wrap container'>
            @hassub('title')
                <h2 class='title text-center'>
                    @sub('title')
                </h2>
            @endsub

            <div class="cases__grid grid grid-cols-3 gap-10">
                @fields('cases')
                    <div class="case">
                        @hassub('image')
                            <img src='@sub('image', 'url')' class='image'>
                        @endsub
                        @hassub('text')
                            <div class='text'>
                                @sub('text')
                            </div>
                        @endsub
                    </div>
                @endfields
            </div>
        </div>
    </section>
@endgroup