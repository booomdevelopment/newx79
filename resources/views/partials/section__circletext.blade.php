@group('section__circletext')
    <section class='circletext'>
        <div class='circletext__wrap container flex items-center justify-center'>
            <div class="circles outer">
                <div class="circles">
                    <div class="circles">
                        <div class="circles inner">
                            @hassub('title')
                                <h2 class='title text-center'>
                                    @sub('title')
                                </h2>
                            @endsub
                            @hassub('text')
                                <div class='text'>
                                    @sub('text')
                                </div>
                            @endsub
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endgroup