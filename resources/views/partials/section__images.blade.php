@group('section__images')
    <section class='images'>
        <div class='images__wrap container flex items-center justify-between'>
            @group('info')
                <div class="info">
                    @hassub('title')
                        <h2 class='title mb-8'>
                            @sub('title')
                        </h2>
                    @endsub
                    @hassub('text')
                        <div class='text text--line mb-8'>
                            @sub('text')
                        </div>
                    @endsub
                    @hassub('cta')
                        <a href='@sub('cta', 'url')' class='btn btn--image-arrow ml-16'>
                            @hasoption('person')
                                <img src="@option('person', 'url')" class="person" alt="">
                            @endoption
                            @sub('cta', 'title')
                            <img src="@asset('images/arrowwhite.svg')" alt="">
                        </a>
                    @endsub
                </div>
            @endgroup

            <div class="images flex flex-col items-center">
                @hassub('top_image')
                <div class="top_image">
                    <img src="@sub('top_image', 'url')" alt="">
                </div>
                @endsub

                <div class="middle-images flex items-center justify-around">
                    @hassub('left_image')
                    <div class="left_image">
                        <img src="@sub('left_image', 'url')" alt="">
                    </div>
                    @endsub
                    @hassub('right_image')
                    <div class="right_image">
                        <img src="@sub('right_image', 'url')" alt="">
                    </div>
                    @endsub
                </div>

                @hassub('bottom_image')
                <div class="bottom_image">
                    <img src="@sub('bottom_image', 'url')" alt="">
                </div>
                @endsub
            </div>
        </div>
    </section>
@endgroup