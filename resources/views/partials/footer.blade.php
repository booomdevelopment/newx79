@options('footer')
    <footer class="site-footer">
        <div class="site-footer__wrap container flex justify-between gap-4">
            @group('logogroup')
                <div class="site-footer__block">
                    @hassub('logo')
                    <a class="site-logo" href="{{ home_url('/') }}">
                        <img src="@sub('logo', 'url')" alt="">
                        
                    </a>
                    @endsub
    
                    @hassub('title')
                    <p class="title">@sub('title')</p>
                    @endsub
                    <div class="links flex items-start flex-wrap">
                        @fields('links')
                            <a href="@sub('link', 'url')" class="icon mr-2"><img src="@sub('icon', 'url')" alt=""></a>
                        @endfields
                    </div>
                </div>
            @endgroup
       
            @hassub('info_1')
                <div class='info_1 site-footer__block'>
                    @sub('info_1')
                </div>
            @endsub

            @hassub('info_2')
                <div class='info_2 site-footer__block'>
                    @sub('info_2')
                </div>
            @endsub
        </div>
    </footer>
@endoptions