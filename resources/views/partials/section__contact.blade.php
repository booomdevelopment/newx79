@group('section__contact')
    <section class='contact' id='contact'>
        <div class='contact__wrap container flex items-center'>
            @group('info')
                <div class="info">
                    @hassub('label')
                        <p class='label mb-6 flex items-center'>
                            <img src="@asset('images/star.svg')" class="mr-2" alt="">
                            @sub('label')
                        </p>
                    @endsub
                    @hassub('title')
                        <h2 class='title mb-4'>
                            @sub('title')
                        </h2>
                    @endsub
                    @hassub('text')
                        <div class='text text--line'>
                            @sub('text')
                        </div>
                    @endsub
                </div>
            @endgroup
            @hassub('form')
                <div class='form'>
                    <?= do_shortcode(get_sub_field('form')); ?>
                </div>
            @endsub
        </div>
    </section>
@endgroup