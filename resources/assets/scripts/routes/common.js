/* eslint-disable */
import Swiper, { Navigation, Pagination } from 'swiper'; 

export default {
  init() {
    // JavaScript to be fired on all pages

    //swiper

    if ($('.image-slider__slider').length) {

      Swiper.use([Navigation, Pagination]);

      new Swiper('.image-slider__slider', {
        slidesPerView: 1,
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
      });
    }

    if ($('.companies-slider__swiper').length) {

      Swiper.use([Navigation, Pagination]);

      new Swiper('.companies-slider__swiper', {
        slidesPerView: 1,
        spaceBetween: 32,

        navigation: {
          nextEl: ".companies-next",
          prevEl: ".companies-prev",
        },

        breakpoints: {
          946: {
            slidesPerView: 3,
            spaceBetween: 60,
          }
        }
      });
    }



  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
